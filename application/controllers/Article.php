<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('general_model', 'model');
	}

	// page home article
	public function index() {
		$this->load->view('article/index');
	}

	// page create article
	public function create() {
		$this->load->view('article/create');
	}
}
