<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';

// custom routes
$route['admin'] = 'admin';

// custom routes article
$route['admin/article']         = 'article';
$route['admin/article/create']  = 'article/create';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
