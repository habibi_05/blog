<?php

class General_model extends CI_Model {

  /**
   * Get all data
   * @table   String    article
   * @where   Array     ['id' => 1]
   * $con     String    result
   */
  public function getData($table)
  {
    $query = $this->db->get($table);
    return $query->result();
  }

  /**
   * Insert data
   * @table   String    article
   * @data    Array     ['title' => 'sample title']
   */
  public function insertData($table, $data)
  {
    $query = $this->db->insert($table, $data);
    return $query;
  }

  /**
   * Update data
   * @id      Int       1
   * @table   String    article
   * @data    Array     ['title' => 'sample title']
   */
  public function updateData($table, $id, $data)
  {
    $this->db->where('id', $id);
    $query = $this->db->update($table, $data);
    return $query;
  }

  /**
   * Delete data
   * @id      Int       1
   * @table   String    article
   */
  public function deleteData($table, $id)
  {
    return $this->db->delete($table, ['id' => $id]);
  }

}
